

#import "CityInfo.h"
#import "Constants.h"

@implementation CityInfo
@synthesize cityName = _cityName;
@synthesize cityId = _cityId;
@synthesize cityTemperature = _cityTemperature;
@synthesize cityHumidity = _cityHumidity;
@synthesize cityDescription = _cityDescription;



-(id) initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self) {
        if (dictionary) {
            [self updateWithDictionary:dictionary];
        }
    }
    return self;
}

- (void) updateWithDictionary:(NSDictionary *)dictionary {
    _cityId = [dictionary objectForKey:kCityID] != [NSNull null] ? [dictionary objectForKey:kCityID] : nil;
    _cityName = [dictionary objectForKey:kCityName] != [NSNull null] ? [dictionary objectForKey:kCityName] : @"";
    _cityHumidity = [dictionary objectForKey:kCityHumidity] != [NSNull null] ? [dictionary objectForKey:kCityHumidity] : nil;
    _cityTemperature = [dictionary objectForKey:kCityTemperature]  != [NSNull null] ? [dictionary objectForKey:kCityTemperature] : nil;
    _cityDescription = [dictionary objectForKey:kCityDescription]  != [NSNull null] ? [dictionary objectForKey:kCityDescription]  : @"";
}

- (NSDictionary *)getAsDictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:_cityId ? _cityId : [NSNull null] forKey:kCityID];
    [dictionary setObject:_cityName ? _cityName : @"" forKey:kCityName];
    [dictionary setObject:_cityHumidity ? _cityHumidity : [NSNull null] forKey:kCityHumidity];
    [dictionary setObject:_cityTemperature ? _cityTemperature : [NSNull null] forKey:kCityTemperature];
    [dictionary setObject:_cityDescription ? _cityDescription : @"" forKey:kCityDescription];
    return dictionary;
}



@end
