//
//  main.m
//  WeatherApp
//
//  Created by Mac One on 11/22/15.
//  Copyright © 2015 Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
