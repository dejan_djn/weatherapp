

#import "WeatherForSelectedCityViewController.h"
#import "AFNetworking/AFHTTPRequestOperation.h"
#import "Constants.h"
#import "DataStorage.h"

@interface WeatherForSelectedCityViewController ()

@end

@implementation WeatherForSelectedCityViewController
@synthesize nameSelectCity = _nameSelectCity;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self prepareInitalData];

    // Do any additional setup after loading the view.
}

-(void)prepareInitalData
{
    NSString *baseURLString = [NSString stringWithFormat:@"%@%@&appid=%@", kBaseAddCityURL, _nameSelectCity, kAppId];
    NSURL *url = [NSURL URLWithString:baseURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        int temperature =  [[[responseObject objectForKey:kResponseKeyMain] objectForKey:kCityTemperature] floatValue] - 273.15;
        long cityID = [[responseObject objectForKey:kCityID] longLongValue];
        [dictionary setObject:[NSNumber numberWithLongLong:cityID] forKey:kCityID];
        [dictionary setObject:[responseObject objectForKey:kCityName] forKey:kCityName];
        int hum = [[[responseObject objectForKey:kResponseKeyMain] objectForKey:kCityHumidity] intValue];
        [dictionary setObject:[NSNumber numberWithInt:hum] forKey:kCityHumidity];
        [dictionary setObject:[NSNumber numberWithInt:temperature] forKey:kCityTemperature];
        [dictionary setObject:[[[responseObject objectForKey:kResponseKeyWeather] objectAtIndex:0] objectForKey:kCityDescription] forKey:kCityDescription];
        city = [[CityInfo alloc]initWithDictionary:dictionary];
        [[DataStorage sharedStorage]updateCitiesList:nil updated:city deleted:nil];
        [self reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        if (!alertView.visible) {
            [alertView show];
        }
        [self reloadData];
        
    }];
    
    
    [operation start];
}


-(void)reloadData
{
    if (!city) {
        NSDictionary *cityDictionary = [[DataStorage sharedStorage]getStoredCityByName:_nameSelectCity];
        city = [[CityInfo alloc]initWithDictionary:cityDictionary];
    }
    self.lblCity.text = city.cityName;
    self.lblTemperature.text = [city.cityTemperature stringValue];
    self.lblHumidity.text = [city.cityHumidity stringValue];
    self.lblDescription.text = city.cityDescription;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancel:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
