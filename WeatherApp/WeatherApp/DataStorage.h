
#import <Foundation/Foundation.h>
#import "CityInfo.h"

@interface DataStorage : NSObject {
    NSArray *storedCities;
}

+ (DataStorage *) sharedStorage;
- (void) initializeStorage;
- (void)updateCitiesList:(CityInfo *)added updated:(CityInfo *)updated deleted:(CityInfo *)deleted;
- (NSArray *)getStoredCitiesList;
- (NSString *)getStoredCitiesIDList;
- (NSDictionary*)getStoredCityByName:(NSString*)nameOfCity;

@end
