
#ifndef Constants_h
#define Constants_h

#define TRACE_RUNTIME_CODEFLOW()    NSStringFromSelector(_cmd)

#define kAppId                              @"2d900e7d3f723d5232a7fd4fbb40abdc"
#define kBaseAddCityURL                     @"http://api.openweathermap.org/data/2.5/weather?q="
#define kBaseListOfCitiesURL                @"http://api.openweathermap.org/data/2.5/group?id="

#define _CityInfoStorageKey_                @"$city_info_storage$"

#define kCityID                             @"id"
#define kCityName                           @"name"
#define kCityHumidity                       @"humidity"
#define kCityTemperature                    @"temp"
#define kCityDescription                    @"description"

#define kResponseKeyMain                    @"main"
#define kResponseKeyWeather                 @"weather"
#define kResponseKeyList                    @"list"


#endif /* Constants_h */
