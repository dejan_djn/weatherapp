//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Mac One on 11/22/15.
//  Copyright © 2015 Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

