
#import "DataStorage.h"
#import "Constants.h"


@implementation DataStorage

static DataStorage *sharedInstance;

+ (DataStorage *) sharedStorage {
    if (!sharedInstance) {
        sharedInstance = [[DataStorage alloc] init];
    }
    return sharedInstance;
}

- (void) initializeStorage {
    NSString *cityList = [[NSUserDefaults standardUserDefaults] objectForKey:_CityInfoStorageKey_];
    if (!cityList) {
        [[NSUserDefaults standardUserDefaults] setObject:[self serializeArray:[NSArray array]] forKey:_CityInfoStorageKey_];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) updateCitiesList:(CityInfo *)added updated:(CityInfo *)updated deleted:(CityInfo *)deleted
{
    if (!added && !updated && !deleted) {
        return;
    }
    
    NSArray *citiesArray = [self getStoredCitiesList];
    
    NSArray *updatedCitiesArray = [self updateArrayListCity:citiesArray addedCity:added updatedCity:updated deleted:deleted];
    if (storedCities) {
        storedCities = nil;
    }
    storedCities = [updatedCitiesArray copy];
    
    [[NSUserDefaults standardUserDefaults] setObject:[self serializeArray:updatedCitiesArray] forKey:_CityInfoStorageKey_];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)updateArrayListCity:(NSArray *)list addedCity:(CityInfo*)addedCity updatedCity:(CityInfo *)updatedCity deleted:(CityInfo *)deletedCity
{
    BOOL breakFor = NO;
    NSMutableArray *mutableObjectArray = [[NSMutableArray alloc] init];
    if (addedCity) {
        if (list) {
            mutableObjectArray = [list mutableCopy];
            for (NSDictionary *objectCityDictionary in mutableObjectArray) {
                CityInfo *objectCityInfo =  [[CityInfo alloc]initWithDictionary:objectCityDictionary];
                if (objectCityInfo.cityId == addedCity.cityId) {
                    breakFor = YES;
                    break;
                } else {
                    [mutableObjectArray addObject:[addedCity getAsDictionary]];
                    break;
                }
            }
        } else {
            [mutableObjectArray addObject:[addedCity getAsDictionary]];
        }
    }
    if (updatedCity) {
        mutableObjectArray = [list mutableCopy];
        for (int i = 0; i < [mutableObjectArray count]; i++) {
            NSDictionary *objectDict = [mutableObjectArray objectAtIndex:i];
            CityInfo *objectCityInfo = [[CityInfo alloc]initWithDictionary:objectDict];
            if (objectCityInfo.cityId == updatedCity.cityId) {
                [mutableObjectArray replaceObjectAtIndex:i withObject:[updatedCity getAsDictionary]];
            }
        }
    }
    if (deletedCity) {
        mutableObjectArray = [list mutableCopy];
        for (int i = 0; i < [mutableObjectArray count]; i++) {
            NSDictionary *objectDict = [mutableObjectArray objectAtIndex:i];
            CityInfo *objectCityInfo = [[CityInfo alloc]initWithDictionary:objectDict];
            if (objectCityInfo.cityId == deletedCity.cityId) {
                [mutableObjectArray removeObject:objectDict];
                if ([mutableObjectArray count] == 0) {
                    mutableObjectArray = nil;
                }
                break;
            }
        }
    }
    return mutableObjectArray ;
}

- (void) clearCities {
    if (storedCities) {
        storedCities = nil;
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:_CityInfoStorageKey_];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (NSArray *)getStoredCitiesList
{
    if (storedCities) {
        return storedCities;
    } else {
        NSArray *cityList = [self deserializeString:[[NSUserDefaults standardUserDefaults] objectForKey:_CityInfoStorageKey_]];
        storedCities = [cityList copy];
        return storedCities;
    }
}

- (NSDictionary*)getStoredCityByName:(NSString*)nameOfCity
{
    NSArray *userCityList = [self getStoredCitiesList];
    for (NSDictionary *cityDictionary in userCityList) {
        if ([[cityDictionary objectForKey:kCityName] isEqualToString:nameOfCity]) {
            return cityDictionary;
        }
    }
    return nil;
}

- (NSString *)getStoredCitiesIDList
{
    NSArray *userCityList = [self getStoredCitiesList];
    NSMutableArray *cityIDList = [NSMutableArray array];
    for (NSDictionary *cityDictionary in userCityList) {
        CityInfo *city = [[CityInfo alloc]initWithDictionary:cityDictionary];
        [cityIDList addObject:city.cityId];
    }
    return [self getStringFromArray:cityIDList];
}

- (NSString*)getStringFromArray:(NSArray*)array
{
    NSMutableString *res = [NSMutableString string];
    BOOL first = YES;
    for(id item in array) {
        if (item == [NSNull null]) continue;
        if (!first) {
            [res appendString:@","];
        } else {
            first = NO;
        }
        [res appendFormat:@"%@", item];
    }
    return res;
}

#pragma mark -
#pragma mark Serialize and Deserialize Functions


- (NSString *)serializeArray:(NSArray *)arrayToSerialize
{
    if (arrayToSerialize && arrayToSerialize.count > 0) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrayToSerialize options:NSJSONWritingPrettyPrinted error:&error];
        NSString *serializedResult = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return (error ? nil : serializedResult);
    }
    return nil;
}

-(NSArray*)deserializeString:(NSString *)stringToDeserialize
{
    if (stringToDeserialize) {
        NSError *error;
        NSData *objectData = [stringToDeserialize dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&error];
        return (error ? nil : json);
    }
    return nil;
}




@end
