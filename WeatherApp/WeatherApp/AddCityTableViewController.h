

#import <UIKit/UIKit.h>
@class AddCityTableViewController;
@class CityInfo;

@protocol AddCityDelegate <NSObject>
- (void)addCityTableViewControllerDidCancel:(AddCityTableViewController *)controller;
- (void)addCityTableViewController:(AddCityTableViewController *)controller didAddCity:(NSString *)cityName;
@end
@interface AddCityTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *nameCity;
@property (nonatomic, weak) id <AddCityDelegate> delegate;
- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;


@end
