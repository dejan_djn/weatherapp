

#import <Foundation/Foundation.h>

@interface CityInfo : NSObject

@property(nonatomic, retain)NSNumber *cityId;
@property(nonatomic, retain)NSString *cityName;
@property(nonatomic, retain)NSNumber *cityTemperature;
@property(nonatomic, retain)NSNumber *cityHumidity;
@property(nonatomic, retain)NSString *cityDescription;

-(id) initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)getAsDictionary;
@end
