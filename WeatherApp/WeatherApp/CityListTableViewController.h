
#import <UIKit/UIKit.h>
#import "AddCityTableViewController.h"

@interface CityListTableViewController : UITableViewController<AddCityDelegate>
@property (nonatomic, strong) NSMutableArray *cities;

@end
