

#import <UIKit/UIKit.h>
#import "CityInfo.h"
@interface WeatherForSelectedCityViewController : UIViewController {

    CityInfo *city;
}

@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblHumidity;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (nonatomic, strong) NSString  *nameSelectCity;

- (IBAction)cancel:(id)sender;
@end
