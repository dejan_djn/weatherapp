

#import "CityListTableViewController.h"
#import "CityInfo.h"
#import "WeatherForSelectedCityViewController.h"
#import "AFNetworking.h"
#import "DataStorage.h"
#import "Constants.h"
@interface CityListTableViewController ()

@end

@implementation CityListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(pullToRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [[DataStorage sharedStorage]initializeStorage];
    [self prepareCities];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) prepareCities {
    NSArray *cityArray = [[DataStorage sharedStorage] getStoredCitiesList];
    if(cityArray && [cityArray count] > 0)
    {
        NSMutableArray *mutable = [NSMutableArray array];
        for (NSDictionary *dict in cityArray) {
            CityInfo *city = [[CityInfo alloc] initWithDictionary:dict];
            [mutable addObject:city];
            
        }
        _cities = [mutable copy];
    } else {
        _cities = nil;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.cities count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCell"];
    
    CityInfo *city = (self.cities)[indexPath.row];
    cell.textLabel.text = city.cityName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",city.cityTemperature];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

[self performSegueWithIdentifier:@"CityWeather" sender:self];
}

#pragma mark addcity delegate

- (void)addCityTableViewControllerDidCancel:(AddCityTableViewController *)controller{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addCityTableViewController:(AddCityTableViewController *)controller didAddCity:(NSString *)cityName {

    
    NSString *baseURLString = [NSString stringWithFormat:@"%@%@&appid=%@", kBaseAddCityURL, cityName, kAppId];
    NSURL *url = [NSURL URLWithString:baseURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        int temperature =  [[[responseObject objectForKey:kResponseKeyMain] objectForKey:kCityTemperature] floatValue] - 273.15;
        long cityID = [[responseObject objectForKey:kCityID] longLongValue];
        [dictionary setObject:[NSNumber numberWithLongLong:cityID] forKey:kCityID];
        [dictionary setObject:[responseObject objectForKey:kCityName] forKey:kCityName];
        [dictionary setObject:[[responseObject objectForKey:kResponseKeyMain] objectForKey:kCityHumidity] forKey:kCityHumidity];
        [dictionary setObject:[NSNumber numberWithInt:temperature] forKey:kCityTemperature];
        [dictionary setObject:[[[responseObject objectForKey:kResponseKeyWeather] objectAtIndex:0] objectForKey:kCityDescription] forKey:kCityHumidity];
        
        CityInfo *cityInfo = [[CityInfo alloc]initWithDictionary:dictionary];
        [[DataStorage sharedStorage]updateCitiesList:cityInfo updated:nil deleted:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self refreshData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        if (!alertView.visible) {
            [alertView show];
        }
        
    }];
    [operation start];

}

- (void)refresh {
    NSString *allCitiesID = [[DataStorage sharedStorage] getStoredCitiesIDList];
    NSString *baseURLString = [NSString stringWithFormat:@"%@%@&units=metric&appid=%@",kBaseListOfCitiesURL, allCitiesID, kAppId];
    NSURL *url = [NSURL URLWithString:baseURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *listofCities = [responseObject objectForKey:kResponseKeyList];
        for (int i = 0; i < [listofCities count]; i++) {
            NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
            int temperature =  [[[[listofCities objectAtIndex:i] objectForKey:kResponseKeyMain] objectForKey:kCityTemperature] floatValue];
            long cityID = [[[listofCities objectAtIndex:i] objectForKey:kCityID] longLongValue];
            [dictionary setObject:[NSNumber numberWithLongLong:cityID] forKey:kCityID];
            [dictionary setObject:[[listofCities objectAtIndex:i] objectForKey:kCityName] forKey:kCityName];
            [dictionary setObject:[[[listofCities objectAtIndex:i] objectForKey:kResponseKeyMain] objectForKey:kCityHumidity] forKey:kCityHumidity];
            [dictionary setObject:[NSNumber numberWithInt:temperature] forKey:kCityTemperature];
            [dictionary setObject:[[[[listofCities objectAtIndex:i] objectForKey:kResponseKeyWeather] objectAtIndex:0] objectForKey:kCityDescription] forKey:kCityHumidity];
            
            CityInfo *cityInfo = [[CityInfo alloc]initWithDictionary:dictionary];
            [[DataStorage sharedStorage]updateCitiesList:nil updated:cityInfo deleted:nil];
            
        }
        [self refreshData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        if (!alertView.visible) {
            [alertView show];
        }
        
    }];
    
    [operation start];
}


-(void)refreshData
{
    [self prepareCities];
    [self.tableView reloadData];
}


- (void)pullToRefresh {
    
    [self refresh];
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[DataStorage sharedStorage]updateCitiesList:nil updated:nil deleted:[_cities objectAtIndex:indexPath.row]];
        [self refreshData];

    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"AddCity"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        AddCityTableViewController *addCity = [navigationController viewControllers][0];
        addCity.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"CityWeather"]) {
    
        UINavigationController *navigationController = segue.destinationViewController;
        WeatherForSelectedCityViewController *city = [navigationController viewControllers][0];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        CityInfo *selectSity = (self.cities)[indexPath.row];
        
        city.nameSelectCity = selectSity.cityName;
    
    }
}


@end
